package org.jash.shop2111.category

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.jash.shop2111.App
import org.jash.shop2111.BR
import org.jash.shop2111.R
import org.jash.shop2111.databinding.FragmentCategoryBinding
import org.jash.shop2111.entry.Category
import org.jash.shop2111.entry.Goods
import org.jash.shop2111.mvvm.BaseFragment
import org.jash.shop2111.utils.CommonAdapter
import org.jash.shop2111.utils.Event


/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class CategoryFragment : BaseFragment<FragmentCategoryBinding, CategoryViewModel>() {
    val categoryAdapter by lazy { CommonAdapter<Category>({ R.layout.category_item to BR.category })}
    val subcategoryAdapter by lazy { CommonAdapter<Category>({ R.layout.sub_category_item to BR.category })}
//    val productAdapter by lazy { CommonAdapter<Goods>({ R.layout.goods_item to BR.goods })}
    val productAdapter by lazy { ProductAdapter(ProductCallback) }
    var cid:Int = 0
    override fun initData() {
        viewModel.categoriesLiveData.observe(this) {
            categoryAdapter.clear()
            categoryAdapter += it
            it[0].showSubcategory()
            Log.d("TAG", "initData: ")
        }
//       viewModel.goodsLiveData.observe(this) {
//            productAdapter += it
//        }
        viewModel.loadCategories()
        Log.d("TAG", "initData: 1")
    }

    override fun initView() {
        binding.categories.adapter = categoryAdapter
        binding.subCategories.adapter = subcategoryAdapter
        binding.product.adapter = productAdapter
        binding.refresh.setOnRefreshListener {
            showProduct(cid)
        }
    }
    @Event("showSubcategory")
    fun showSubcategory(list:List<Category>) {
        subcategoryAdapter.clear()
        subcategoryAdapter += list
        if (list.isNotEmpty()) {
            list[0].showProduct()
        }
    }
    @Event("showProduct")
    fun showProduct(id:Int) {
        cid = id
//        productAdapter.clear()
//        viewModel.loadGoods(id, 1, 10)
        lifecycleScope.launch {
            viewModel.getPagingSource(id).collect {
                productAdapter.submitData(it)
                binding.refresh.isRefreshing = false
            }
        }
    }

    override fun error(throwable: Throwable) {
        super.error(throwable)
        binding.refresh.isRefreshing = false
    }

//    override val defaultViewModelProviderFactory: ViewModelProvider.Factory
//        get() = viewModelFactory { initializer { CategoryViewModel((requireContext().applicationContext as App).database.getCategoryDao()) } }
}