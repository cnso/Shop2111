package org.jash.shop2111.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.DifferCallback
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import org.jash.shop2111.BR
import org.jash.shop2111.R
import org.jash.shop2111.entry.Goods
import org.jash.shop2111.utils.CommonViewHolder

class ProductAdapter(callback: DiffUtil.ItemCallback<Goods>):PagingDataAdapter<Goods, CommonViewHolder>(callback) {
    override fun onBindViewHolder(holder: CommonViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.setVariable(BR.goods, item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommonViewHolder =
        CommonViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.goods_item, parent, false))
}