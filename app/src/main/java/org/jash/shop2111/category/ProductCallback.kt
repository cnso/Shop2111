package org.jash.shop2111.category

import androidx.recyclerview.widget.DiffUtil
import org.jash.shop2111.entry.Goods

object ProductCallback: DiffUtil.ItemCallback<Goods>() {
    override fun areItemsTheSame(oldItem: Goods, newItem: Goods): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Goods, newItem: Goods): Boolean = oldItem == newItem
}