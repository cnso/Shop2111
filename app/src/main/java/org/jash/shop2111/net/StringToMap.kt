package org.jash.shop2111.net

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter

class StringToMap: TypeAdapter<Map<String, List<String>>?> (){

    val gson by lazy { Gson() }
    val type = object :TypeToken<Map<String, List<String>>>(){}.type
    override fun write(out: JsonWriter?, value: Map<String, List<String>>?) {
        out?.value(gson.toJson(value))
    }

    override fun read(`in`: JsonReader?): Map<String, List<String>>? {
        val s = `in`?.nextString()
        return gson.fromJson(s, type)
    }
}