package org.jash.shop2111.net

import io.reactivex.rxjava3.core.Observable
import org.jash.shop2111.entry.BannerEntry
import org.jash.shop2111.entry.CartItem
import org.jash.shop2111.entry.Category
import org.jash.shop2111.entry.Goods
import org.jash.shop2111.entry.Res
import org.jash.shop2111.entry.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface APIService {

    @POST("/user/loginjson")
    fun login(@Body user:Map<String, String>):Observable<Res<User>>

    @GET("/goods/info")
    fun getGoodsList(@Query("category_id") category_id:Int, @Query("currentPage") currentPage:Int, @Query("pageSize") pageSize:Int):Observable<Res<List<Goods>>>
    @GET("/goods/info")
    suspend fun getGoods(@Query("category_id") category_id:Int, @Query("currentPage") currentPage:Int, @Query("pageSize") pageSize:Int):Res<List<Goods>>
    @GET("/goods/category")
    suspend fun getCategories():Res<List<Category>>
    @GET("/banner/json")
    suspend fun getBanner():Res<List<BannerEntry>>
    @GET("/goods/detail")
    suspend fun getGoodsDetail(@Query("goods_id") id:Int) : Res<Goods>
    @POST("/goods/addCar")
    suspend fun addCart(@Body map: Map<String, Int>):Res<Any?>
    @GET("/goods/selectCar")
    suspend fun selectCart():Res<List<CartItem>>
    @POST("/goods/deleteCar")
    suspend fun deleteCart(@Body map:Map<String, String>):Res<Any?>
}