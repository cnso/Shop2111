package org.jash.shop2111.net

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type

class MyConverterFactory: Converter.Factory() {
    private val factory = GsonConverterFactory.create()

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? = factory.responseBodyConverter(type, annotations, retrofit)

    override fun requestBodyConverter(
        type: Type,
        parameterAnnotations: Array<out Annotation>,
        methodAnnotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<*, RequestBody>?  =
        factory.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit)
            .let { converter -> methodAnnotations.find { it is Sign }?.let { MyRequestBodyConverter(converter) }  ?: converter}

}