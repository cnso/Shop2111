package org.jash.shop2111.cart

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.jash.shop2111.entry.CartItem
import org.jash.shop2111.mvvm.BaseViewModel
import org.jash.shop2111.utils.SingleLiveData
import org.jash.shop2111.utils.dao.CartItemDao
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(val cartItemDao: CartItemDao):BaseViewModel() {
    val cartsLiveData by lazy { SingleLiveData<List<CartItem>>() }
    val deleteLiveData by lazy { SingleLiveData<String>() }
    fun loadCarts() {
        viewModelScope.launch {
            try {
                val res = service.selectCart()
                if (res.code == 200) {
                    cartsLiveData.postValue(res.data)
                    cartItemDao.save(*res.data.toTypedArray())
                } else {
                    errorLiveData.postValue(RuntimeException(res.message))
                }
            } catch (e:Exception) {
                errorLiveData.postValue(e)
            }
        }
    }
    fun deleteCart(id:Int) {
        viewModelScope.launch {
            try {
                val res = service.deleteCart(mapOf("ids" to id.toString()))
                if (res.code == 200) {
                    deleteLiveData.postValue(res.message)
                    cartItemDao.delete(id)
                } else {
                    errorLiveData.postValue(RuntimeException(res.message))
                }
            } catch (e:Exception) {
                errorLiveData.postValue(e)
            }
        }
    }
}