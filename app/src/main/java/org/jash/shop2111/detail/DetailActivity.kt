package org.jash.shop2111.detail

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.databinding.DataBindingUtil
import com.alibaba.android.arouter.facade.annotation.Autowired
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import com.hyphenate.helpdesk.easeui.util.IntentBuilder
import dagger.hilt.android.AndroidEntryPoint
import org.jash.shop2111.BR
import org.jash.shop2111.R
import org.jash.shop2111.databinding.ActivityDetailBinding
import org.jash.shop2111.databinding.ConfigItemBinding
import org.jash.shop2111.entry.Goods
import org.jash.shop2111.mvvm.BaseActivity
import org.jash.shop2111.utils.bus

@Route(path = "/shop/detail")
@AndroidEntryPoint
class DetailActivity : BaseActivity<ActivityDetailBinding, DetailViewModel>() {
    @Autowired
    @JvmField
    var id:Int = 0
    lateinit var goods: Goods
    override fun initData() {
        ARouter.getInstance().inject(this)
        viewModel.addCartLiveData.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }
        viewModel.goodsLiveData.observe(this) {
            goods = it
            binding.goods = it
            binding.setVariable(BR.goods, it)
            it.goods_attribute?.forEach { (k, v) ->
                val config = DataBindingUtil.inflate<ConfigItemBinding>(
                    layoutInflater,
                    R.layout.config_item,
                    binding.config,
                    true
                )
                config.text.hint = k
                config.auto.setSimpleItems(v.toTypedArray())
            }
        }
        viewModel.loadGoods(id)
    }

    override fun initView() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.addCart.setOnClickListener {
            viewModel.addCart(goods)
        }
        binding.call.setOnClickListener {
            val intent = IntentBuilder(this).setServiceIMNumber("kefuchannelimid_972191")
                .build()
            startActivity(intent)
        }
    }

}