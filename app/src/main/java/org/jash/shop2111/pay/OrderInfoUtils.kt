package org.jash.shop2111.pay

import android.os.Build
import android.util.Base64
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.security.KeyFactory
import java.security.Signature
import java.security.spec.PKCS8EncodedKeySpec
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.UUID

object OrderInfoUtils {
    private val APPID = "2021000122688185"
    private val RSA_PRIVATE = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCWcpkNcLzN8DR76VpObD1erp6H42npymuKiYAMYopRBiQs/ONzX5xEFHLX7PgtrAq3SFK/rwqkq9t1syT3SridvzyEmvUbpTRlZsT3vI6bbQA+vvZpROFWHDtfHE46yQCQ6TN1b4An53HIbbH8B+1KjBdZ5Rx47r/U2m119knwXvYk6nfbAosZIIs8N3+NxbxJ98v12PU/eQiiBSQGjeSL9CguvHL2C3Zb6j+tu8XJ9IIDbyjj6BkxK/vumcylD3qDNp9bpbFxudWSOq/DOtAFykUeW6PV9617ODpSxR9EHjRWZNfOITBaOScyUgcorvQET3bKVK4jqipd7fGjxs4dAgMBAAECggEBAIuwviG3oucIeILxKdOVNupZ8oK0Clx0gaPxhb56JHmU3TQUX1B53wtrfTK2Kb2md5C0i3jFm3lIxQ2lF9axP3fg3900UA+qNltMr+hi4Hq6G7cm8n7N4PhmYE4wZwa4KVlj3k7VB8aFZKW4DMG5zn8gUnI6hFRZQi7t3SLhWsfexm6X50iTa4+JV+mNpnjhzpjaKREPU1v5DybXt6vFAzGvPp4gylgxiR70i7ZWvtniurXsyAQ/6QPoDu2/pL0MYwAiVMxKf0k3swNtZD/slq43kjwEVQZtPcF60PDzSUOw6ck7954c9nVZtB77PquUZa/hKDWkSll/cvSC3soCI+ECgYEA1xFp7LIIQ+j2Ykd1hQZ3iiGnkKFRkvAqmZJQwEG8tVzkTz2s0fwrvm0wzR8X6mlk/okx/C2U7XpvhJEfF6o09o4hT+OhlZvUsnJip/AgINZL5CjERiSk1ygOqjaRbJyLOCXYA/nrwdyUD6gKAgjaRtMTPOnW74vmYqC2AqlHDMkCgYEAsxS80Vj8JTmpCGCl5IHCa65iDY2MgI6NeJC72uASRm7XpAMIGKcQWZ0ge4KaF3nHc5bQpaEtNtAJabbZsde/9wMokO5oeyaEnXadniXRlc9Ki/CUDyHY1nTZfluMyd5v1n14zYJ7LDzZGMykFmDC42K9WeBN0YRMwhbcpyyKpLUCgYEAixT52VYwvioT2+62czaBa9Tj1kZcyJeEFRhoHEzmyqquAwixkIJxj45xdtVa0/8gjvMOXjTkEMeTAroOz3EFG0VDCbT9tBC4dkY+wrgpLtMm9P/gHjRoXbPL+Aud9A0wgzkF/1xKOG57Wbyj7DjpX8/0qvnWQ8fHT5T637G0/vECgYEAmImOd8eTJuYha+hfBe/dOjmUfbTXO92w9URbLwlzp5jch3Cpoc4T6DElrO7G8D4jvPp2iM5sI+c6TViZv5llgzKmiCT6zAM4LKQPh9jw1+l0txRaGTWcbLelVrMGjiCxJINFtc9xuXGu/ie5kPIkrui5HugngNJvS1eL5b3T7o0CgYBvvYXTgwSkc8BSmEUVpWIlHhg90MU97UPjxc9KN6mOCNbCNLNEmnb94U2rRwrbSXsFSVAhzXT4tDfW0aSdBpglmXbUvMgOuGswUcPRFwBUjDawygeTcX10rjQxnp47eVFIPkzFLEaAOZnWyNGl79DX+a9IF7UJ2mF057oCEkUPWA=="

    private val gson = Gson()
    private val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

    fun buildOrderParamMap(amount:Float, body:String):Map<String, String> {
        val map = mapOf("timeout_express" to "30m",
                "product_code" to "QUICK_MSECURITY_PAY",
                "total_amount" to amount.toString(),
                "subject" to "1",
                "body" to body,
                "out_trade_no" to getOutTradeNo()
            )
        return mapOf(
            "app_id" to APPID,
            "biz_content" to gson.toJson(map),
            "charset" to "utf-8",
            "method" to  "alipay.trade.app.pay",
            "sign_type" to "RSA",
            "timestamp" to sdf.format(Date()),
            "version" to "1.0"
        )
    }
    fun getSign(map: Map<String, String>): String {
        val info = map.toList().sortedBy { it.first }
            .joinToString("&") { "${it.first}=${it.second}" }
        val signature = Signature.getInstance("SHA1WithRSA")
        val privateKey = KeyFactory.getInstance("RSA")
            .generatePrivate(PKCS8EncodedKeySpec(Base64.decode(RSA_PRIVATE, Base64.DEFAULT)))
        signature.initSign(privateKey)
        signature.update(info.toByteArray())
        val sign = signature.sign()
        return "sign=${Base64.encodeToString(sign, Base64.DEFAULT)}"
    }
    private fun getOutTradeNo(): String {
        val uuid = UUID.randomUUID()
        return uuid.toString()
    }
    fun buildOrderParam(map:Map<String, String>):String {
        return map.toList().joinToString("&") { "${it.first}=${URLEncoder.encode(it.second, "UTF-8")}" }
    }
}