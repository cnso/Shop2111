package org.jash.shop2111.main

import android.util.Log
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.alibaba.android.arouter.facade.annotation.Route
import dagger.hilt.android.AndroidEntryPoint
import org.jash.shop2111.R
import org.jash.shop2111.databinding.ActivityMainBinding
import org.jash.shop2111.entry.User
import org.jash.shop2111.mvvm.BaseActivity
import org.jash.shop2111.utils.Event

@Route(path = "/shop/main")
@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {


    override fun initData() {
    }

    override fun initView() {
        val navController = supportFragmentManager.findFragmentById(R.id.nav)?.findNavController()
        navController?.let { binding.bottom.setupWithNavController(it) }

    }
    @Event("setCartNumber")
    fun setCartNumber(n:Int) {
        val badge = binding.bottom.getOrCreateBadge(R.id.cart)
        badge.number = n
        badge.isVisible = badge.number != 0
    }
    @Event("cartNumberChange")
    fun cartNumberChange(n:Int) {
        val badge = binding.bottom.getOrCreateBadge(R.id.cart)
        badge.number += n
        badge.isVisible = badge.number != 0
    }

}