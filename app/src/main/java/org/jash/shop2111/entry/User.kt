package org.jash.shop2111.entry

data class User(
    val address: String,
    val admin: Boolean,
    val birth: Any,
    val coinCount: Int,
    val email: Any,
    val icon: String,
    val id: Int,
    val nickname: String,
    val password: String,
    val publicName: Any,
    val sex: String,
    val token: String,
    val type: Int,
    val username: String
)