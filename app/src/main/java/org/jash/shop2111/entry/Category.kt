package org.jash.shop2111.entry

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import org.jash.shop2111.utils.bus
@Entity
data class Category (
    val category_icon: String?,
    val category_name: String,
    @PrimaryKey
    val id: Int,
    val parent_id: Int
) {
    @Ignore
    var `data`: List<Category>? = null
    fun showSubcategory() {
        bus.postValue("showSubcategory" to data)
    }
    fun showProduct() {
        bus.postValue("showProduct" to id)
    }
}