package org.jash.shop2111.entry

data class BannerEntry(
    val desc: String,
    val id: Int,
    val imagePath: String,
    val isVisible: Int,
    val order: Int
)