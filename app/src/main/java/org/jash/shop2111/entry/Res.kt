package org.jash.shop2111.entry

data class Res<D> (
    val code:Int,
    val message:String,
    val `data`: D
)