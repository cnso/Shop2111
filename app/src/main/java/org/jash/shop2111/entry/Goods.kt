package org.jash.shop2111.entry

import android.content.Context
import android.widget.Toast
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.alibaba.android.arouter.launcher.ARouter
import com.google.gson.annotations.JsonAdapter
import org.jash.shop2111.net.StringToMap
import org.jash.shop2111.utils.bus
@Entity
data class Goods(
    val bannerList: List<String>,
    val category_id: Int,
    @JsonAdapter(StringToMap::class)
    val goods_attribute: Map<String, List<String>>?,
    val goods_banner: String,
    val goods_code: String,
    val goods_default_icon: String,
    val goods_default_price: Float,
    val goods_desc: String,
    val goods_detail_one: String,
    val goods_detail_two: String,
    val goods_sales_count: Int,
    val goods_stock_count: Int,
    @PrimaryKey
    val id: Int
) {
    fun showDetail(context: Context) {
        ARouter.getInstance().build("/shop/detail")
            .withInt("id", id)
            .navigation()
    }
}