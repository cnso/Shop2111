package org.jash.shop2111.entry

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import org.jash.shop2111.BR
import org.jash.shop2111.utils.bus

@Entity
data class CartItem(
    val goods_default_icon: String,
    val goods_default_price: Float,
    val goods_desc: String,
    val goods_id: Int,
    @PrimaryKey
    val id: Int,
    val order_id: Int,
    val user_id: Int
) : BaseObservable() {
    @Bindable
    var count: Int = 0
        set(value) {
            if (value > 0) {
                field = value
                notifyPropertyChanged(BR.count)
            }

        }
    fun addCount(n:Int) {
        count += n
    }
    fun deleteCart() {
        bus.postValue("deleteCart" to this)
    }
    @Bindable
    @Ignore
    var check = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.check)
        }
}