package org.jash.shop2111

import android.app.Application
import android.util.Log
import androidx.room.Room
import com.alibaba.android.arouter.launcher.ARouter
import com.alipay.sdk.app.EnvUtils
import com.hyphenate.chat.ChatClient
import com.hyphenate.chat.ChatClient.Options
import com.hyphenate.helpdesk.callback.Callback
import com.hyphenate.helpdesk.easeui.UIProvider
import dagger.hilt.android.HiltAndroidApp
import org.jash.shop2111.utils.dao.ShopDatabase
@HiltAndroidApp
class App: Application() {

    val database by lazy {
        Room.databaseBuilder(this, ShopDatabase::class.java, "shop")
            .fallbackToDestructiveMigration()
            .build()
    }
    override fun onCreate() {
        super.onCreate()
        if(BuildConfig.DEBUG) {
            ARouter.openLog()
            ARouter.openDebug()
        }
        ARouter.init(this)
        EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX)
        val options:Options =  Options()
        options.setAppkey("1458240411068158#kefuchannelapp108947")
        options.setTenantId("108947")
        if (BuildConfig.DEBUG) {
            options.setConsoleLog(true)
        }

// Kefu SDK 初始化
        if (!ChatClient.getInstance().init(this, options)){
            return;
        }
// Kefu EaseUI的初始化
        UIProvider.getInstance().init(this);
//        ChatClient.getInstance().register("test02", "test02", object : Callback {
//            override fun onSuccess() {
//                Log.d("APP", "onSuccess: ")
//            }
//
//            override fun onError(code: Int, error: String?) {
//                Log.d("APP", "onError: $code === $error")
//            }
//
//            override fun onProgress(progress: Int, status: String?) {
//                Log.d("APP", "onProgress: $progress === $status")
//            }
//        })

    }
}