package org.jash.shop2111.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.SortedList

class SearchAdapter<D>(val layout:(D) -> Pair<Int, Int>):Adapter<CommonViewHolder>() {
    lateinit var data:SortedList<D>
    override fun getItemViewType(position: Int): Int = layout(data[position]).first

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommonViewHolder =
        CommonViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), viewType, parent, false))

    override fun getItemCount(): Int = data.size()

    override fun onBindViewHolder(holder: CommonViewHolder, position: Int) {
        holder.binding.setVariable(layout(data[position]).second, data[position])
    }
}
class CommonViewHolder(val binding: ViewDataBinding):ViewHolder(binding.root)