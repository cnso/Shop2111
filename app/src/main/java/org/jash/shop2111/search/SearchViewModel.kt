package org.jash.shop2111.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.jash.shop2111.entry.Goods
import org.jash.shop2111.mvvm.BaseViewModel
import org.jash.shop2111.utils.dao.GoodsDao
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(val goodsDao: GoodsDao):BaseViewModel() {
    val goodsLiveData by lazy { MutableLiveData<List<Goods>>() }
    fun search(key:String) {
        viewModelScope.launch {
            val goods = goodsDao.searchGoods("%$key%")
            goodsLiveData.postValue(goods)
        }
    }
}