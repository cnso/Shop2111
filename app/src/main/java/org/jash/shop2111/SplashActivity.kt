package org.jash.shop2111

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jash.shop2111.login.LoginActivity
import org.jash.shop2111.utils.get
import org.jash.shop2111.utils.put

class SplashActivity : AppCompatActivity() {
    private val count by lazy { findViewById<TextView>(R.id.count) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        if (get("first", "") == ""){
            lifecycleScope.launch {
                for (i in 3 downTo 0) {
                    count.text = "倒计时${i}秒"
                    delay(1000)
                }
                put("first", "1")
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
        } else {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }
}