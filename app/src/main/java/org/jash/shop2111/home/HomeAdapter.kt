package org.jash.shop2111.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.alibaba.android.arouter.launcher.ARouter
import org.jash.shop2111.entry.Category

class HomeAdapter(val data:MutableList<Category> = mutableListOf(), f: Fragment) : FragmentStateAdapter(f) {
    override fun getItemCount(): Int = data.size

    override fun createFragment(position: Int): Fragment =
        ARouter.getInstance().build("/shop/product")
            .withInt("cid", data[position].id)
            .navigation() as Fragment
    fun getTitle(position: Int) = data[position].category_name
    fun getIcon(position: Int) = data[position].category_icon
    operator fun plusAssign(list: List<Category>) {
        val size = data.size
        data += list
        notifyItemRangeInserted(size, list.size)
    }
}