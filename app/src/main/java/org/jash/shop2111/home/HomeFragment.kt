package org.jash.shop2111.home

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.alibaba.android.arouter.launcher.ARouter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.tabs.TabLayoutMediator
import com.youth.banner.loader.ImageLoader
import dagger.hilt.android.AndroidEntryPoint
import org.jash.shop2111.App
import org.jash.shop2111.BR
import org.jash.shop2111.R
import org.jash.shop2111.databinding.FragmentHomeBinding
import org.jash.shop2111.entry.BannerEntry
import org.jash.shop2111.entry.Goods
import org.jash.shop2111.mvvm.BaseFragment
import org.jash.shop2111.utils.CommonAdapter

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {
    val adapter by lazy { HomeAdapter(f = this) }
    override fun initData() {
        viewModel.categoriesLiveData.observe(this) {
            adapter += it[0].data ?: listOf()
        }
        viewModel.bannerLiveData.observe(this) {
            binding.banner.update(it.map(BannerEntry::imagePath), it.map(BannerEntry::desc))
        }
        viewModel.loadBanner()
        viewModel.loadCategories()
    }

    override fun initView() {
        binding.pager.isSaveEnabled = false
        binding.pager.adapter = adapter
        TabLayoutMediator(binding.tab, binding.pager) { tab, p ->
            tab.text = adapter.getTitle(p)
            Glide.with(this)
                .load(adapter.getIcon(p))
                .into(object :CustomTarget<Drawable>() {
                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        tab.icon = resource
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }
                })
        }.attach()
        binding.banner.setImageLoader(object : ImageLoader() {
            override fun displayImage(context: Context?, path: Any?, imageView: ImageView?) {
                Glide.with(imageView!!).load(path as String).into(imageView)
            }
        })
        binding.search.editText.setOnEditorActionListener { v, _, event ->
            when(event.action) {
                KeyEvent.ACTION_UP -> {
                    if (v.text.isNotEmpty()) {
                        ARouter.getInstance().build("/shop/search")
                            .withString("key", v.text.toString())
                            .navigation()
                    }
                }
            }
            true
        }
    }
//
//    override val defaultViewModelProviderFactory: ViewModelProvider.Factory
//        get() = viewModelFactory { initializer { HomeViewModel((requireContext().applicationContext as App).database.getCategoryDao()) } }
}