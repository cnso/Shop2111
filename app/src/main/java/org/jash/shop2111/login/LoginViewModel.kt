package org.jash.shop2111.login

import androidx.lifecycle.MutableLiveData
import io.reactivex.rxjava3.schedulers.Schedulers
import org.jash.shop2111.entry.User
import org.jash.shop2111.mvvm.BaseViewModel

class LoginViewModel:BaseViewModel() {
    val loginLiveData by lazy { MutableLiveData<User>() }
    fun login(user:Map<String, String>) {
        val disposable = service.login(user)
            .subscribe({
                if (it.code == 200 && it.data != null) {
                    loginLiveData.postValue(it.data)
                } else {
                    errorLiveData.postValue(RuntimeException(it.message))
                }
            }, errorLiveData::postValue)
        addCloseable { disposable.takeIf { !it.isDisposed }?.dispose() }
    }
}