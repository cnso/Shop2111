package org.jash.shop2111.utils

import android.content.Context
import android.content.SharedPreferences

fun Context.put(key:String, value:String) {
    getSharedPreferences("shop", Context.MODE_PRIVATE).edit()
        .putString(key, value)
        .apply()
}
fun Context.get(key: String, defaultValue: String) =
    getSharedPreferences("shop", Context.MODE_PRIVATE).getString(key, defaultValue) ?: defaultValue
