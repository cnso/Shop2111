package org.jash.shop2111.utils.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import org.jash.shop2111.entry.Goods
@Dao
interface GoodsDao {
    @Upsert
    fun save(vararg goods: Goods):Completable
    @Query("select * from goods where category_id=:categoryId")
    fun findByCategoryId(categoryId:Int):Observable<List<Goods>>
    @Query("select * from goods where goods_desc like :key")
    suspend fun searchGoods(key:String):List<Goods>
}