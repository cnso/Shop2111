package org.jash.shop2111.utils

import android.content.Context
import android.view.View
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.Spinner
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.behavior.SwipeDismissBehavior
import com.google.android.material.checkbox.MaterialCheckBox
import com.youth.banner.Banner
import com.youth.banner.loader.ImageLoader
import com.youth.banner.loader.ImageLoaderInterface

@BindingAdapter("url")
fun loadImage(image:ImageView, url:String?) {
    Glide.with(image)
        .load(url)
        .into(image)
}
@BindingAdapter("image_path")
fun setImagePath(banner:Banner, imagePaths:List<String>?) {
    banner.setImageLoader(object : ImageLoader(){
        override fun displayImage(context: Context?, path: Any?, imageView: ImageView?) {
            Glide.with(imageView!!).load(path as String)
                .into(imageView)
        }
    })
    imagePaths?.let {
        banner.update(it)
    }
}
@InverseBindingAdapter(attribute = "checkedState")
fun getCheckState(checkBox: MaterialCheckBox):Int = checkBox.checkedState
@BindingAdapter("checkedStateAttrChanged")
fun setListener(checkBox: MaterialCheckBox, attrChange:InverseBindingListener) {
    checkBox.addOnCheckedStateChangedListener { _, _ ->
        attrChange.onChange()
    }
}
@BindingAdapter("onSwipe")
fun setOnSwipe(view: View, dismiss:()->Unit) {
    (view.layoutParams as? CoordinatorLayout.LayoutParams)?.let {
        val behavior = SwipeDismissBehavior<View>()
        it.behavior = behavior
        behavior.listener = object :SwipeDismissBehavior.OnDismissListener {
            override fun onDismiss(p0: View?) {
                p0?.alpha = 1f
                dismiss()
            }

            override fun onDragStateChanged(p0: Int) {
            }

        }
    }
}