package org.jash.shop2111.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

/**
 * 解决数据倒灌问题
 */
class SingleLiveData<T>:MutableLiveData<T>() {
    val flag = mutableMapOf<Observer<in T>, AtomicBoolean>()
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        flag[observer] = AtomicBoolean(false)
        super.observe(owner) {
            if (flag[observer]?.compareAndSet(true, false) == true) {
                observer.onChanged(it)
            }
        }
    }

    override fun setValue(value: T) {
        flag.values.forEach { it.set(true) }
        super.setValue(value)
    }
}