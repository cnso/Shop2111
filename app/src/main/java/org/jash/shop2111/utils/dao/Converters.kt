package org.jash.shop2111.utils.dao

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {
    val gson = Gson()
    val type = object :TypeToken<Map<String, List<String>>>(){}.type
    val listType = object :TypeToken<List<String>>(){}.type
    @TypeConverter
    fun fromMap(map: Map<String,List<String>>?):String? =
        map?.let { gson.toJson(it) }
    @TypeConverter
    fun toMap(str:String?):Map<String, List<String>>? =
        str?.let { gson.fromJson(it, type) }
    @TypeConverter
    fun fromList(list: List<String>?):String? =
        list?.let { gson.toJson(it) }
    @TypeConverter
    fun toList(str:String?):List<String>? =
        str?.let { gson.fromJson(it, listType) }
}