package org.jash.shop2111.utils.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import androidx.room.Upsert
import org.jash.shop2111.entry.CartItem
@Dao
interface CartItemDao {
    @Upsert
    suspend fun save(vararg item:CartItem)
    @Query("select * from cartitem where goods_id=:goodsId and user_id=:userId")
    suspend fun findByGoodsId(userId:Int, goodsId:Int):CartItem?
    @Query("select * from cartitem where user_id=:userId")
    suspend fun findAll(userId: Int):List<CartItem>
    @Query("delete from cartitem where id = :id" )
    suspend fun delete(id:Int)

}