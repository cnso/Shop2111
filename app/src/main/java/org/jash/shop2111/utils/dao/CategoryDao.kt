package org.jash.shop2111.utils.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import org.jash.shop2111.entry.Category

@Dao
interface CategoryDao {
    @Upsert
    suspend fun save(vararg category:Category)
    @Query("select * from category")
    suspend fun findAll():List<Category>
    @Query("select * from category where parent_id=:parentId")
    suspend fun findByParentId(parentId:Int):List<Category>
}