package org.jash.shop2111.utils.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.jash.shop2111.entry.CartItem
import org.jash.shop2111.entry.Category
import org.jash.shop2111.entry.Goods

@Database(entities = [Category::class, Goods::class, CartItem::class], version = 4)
@TypeConverters(Converters::class)
abstract class ShopDatabase:RoomDatabase() {
    abstract fun getCategoryDao():CategoryDao
    abstract fun getGodsDao():GoodsDao
    abstract fun getCartItemDao():CartItemDao
}