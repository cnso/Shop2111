package org.jash.shop2111.utils.dao

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DBFactory {
    @Singleton
    @Provides
    fun getDB(@ApplicationContext app:Context) =
        Room.databaseBuilder(app, ShopDatabase::class.java, "shop")
            .fallbackToDestructiveMigration()
            .build()
    @Singleton
    @Provides
    fun getCategoryDao(db:ShopDatabase) =
        db.getCategoryDao()
    @Singleton
    @Provides
    fun getGoodsDao(db:ShopDatabase) =
        db.getGodsDao()
    @Singleton
    @Provides
    fun getCartItemDao(db:ShopDatabase) =
        db.getCartItemDao()
}