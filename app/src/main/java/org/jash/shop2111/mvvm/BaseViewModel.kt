package org.jash.shop2111.mvvm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.jash.shop2111.net.APIService
import org.jash.shop2111.net.service

open class BaseViewModel:ViewModel() {
    val errorLiveData by lazy { MutableLiveData<Throwable>() }
    val service:APIService by lazy { service() }
}